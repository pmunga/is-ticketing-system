<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/user-types')->group(function () {
    Route::get('', 'User\UserTypesController@index');
    Route::post('', 'User\UserTypesController@store');
    Route::get('/{id}', 'User\UserTypesController@show');
    Route::patch('/{id}', 'User\UserTypesController@update');
    Route::delete('/{id}', 'User\UserTypesController@destroy');
});

Route::prefix('/users')->group(function () {
    Route::get('', 'User\UsersController@index');
    Route::post('', 'User\UsersController@store');
    Route::get('/{id}', 'User\UsersController@show');
    Route::patch('/{id}', 'User\UsersController@update');
    Route::delete('/{id}', 'User\UsersController@destroy');
});



Route::prefix('/trains')->group(function () {
    Route::get('', 'TrainController@index');
    Route::post('', 'TrainController@store');
    Route::get('/{id}', 'TrainController@show');
    Route::patch('/{id}', 'TrainController@update');
    Route::delete('/{id}', 'TrainController@destroy');
});
