<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFairRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fair_rates', function (Blueprint $table) {
            $table->uuid('id')->primary('id');

            $table->uuid('departing_station_id');
            $table->foreign('departing_station_id')
                ->references('id')->on('stations')
                ->onDelete('cascade');

            $table->uuid('arrival_station_id');
            $table->foreign('arrival_station_id')
                ->references('id')->on('stations')
                ->onDelete('cascade');

            $table->double('amount');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fair_rates');
    }
}
