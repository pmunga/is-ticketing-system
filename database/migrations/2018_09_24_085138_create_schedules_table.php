<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->uuid('id')->primary('id');

            $table->uuid('station_id');
            $table->foreign('station_id')
                ->references('id')->on('stations')
                ->onDelete('cascade');

            $table->uuid('train_id');
            $table->foreign('train_id')
                ->references('id')->on('trains')
                ->onDelete('cascade');

            $table->dateTime('departure_date_time');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
