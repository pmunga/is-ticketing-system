<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->uuid('id')->primary('id');

            $table->uuid('schedule_id');
            $table->foreign('schedule_id')
                ->references('id')->on('schedules')
                ->onDelete('cascade');

            $table->uuid('from_station');
            $table->foreign('from_station')
                ->references('id')->on('stations')
                ->onDelete('cascade');

            $table->uuid('to_station');
            $table->foreign('to_station')
                ->references('id')->on('stations')
                ->onDelete('cascade');

            $table->uuid('train_id');
            $table->foreign('train_id')
                ->references('id')->on('trains')
                ->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
