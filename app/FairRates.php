<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FairRates extends Model
{
    use SoftDeletes;
    public $incrementing = false;
}
