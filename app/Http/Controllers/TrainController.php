<?php

namespace App\Http\Controllers;

use App\Train;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class TrainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = array(
            'data' => Train::all()
        );
        return response($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'luggage_capacity' => 'required',
            'passenger_capacity' => 'required',
        ));

        $train = new Train();
        $train->id = Uuid::uuid4();
        $train->name = $request->name;
        $train->luggage_capacity = $request->luggage_capacity;
        $train->passenger_capacity = $request->passenger_capacity;
        $train->save();

        $response = array(
            'message' => array(
                'title' => 'Success',
                'type' => 'success',
                'message' => 'Train added successfully.'
            )
        );
        return response($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = array(
            'data' => Train::find($id)
        );
        return response($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'name' => 'required',
            'luggage_capacity' => 'required',
            'passenger_capacity' => 'required',
        ));

        $train = Train::find($id);
        $train->name = $request->name;
        $train->luggage_capacity = $request->luggage_capacity;
        $train->passenger_capacity = $request->passenger_capacity;
        $train->save();

        $response = array(
            'message' => array(
                'title' => 'Updated',
                'type' => 'info',
                'message' => 'Train updated successfully.'
            )
        );
        return response($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
