<?php

namespace App\Http\Controllers\User;

use App\UserType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ramsey\Uuid\Uuid;

class UserTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = array(
            'data' => UserType::all()
        );
        return response($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'description' => 'required',
        ));

        $user_type = new UserType();
        $user_type->id = Uuid::uuid4();
        $user_type->name = $request->name;
        $user_type->description = $request->description;
        $user_type->save();

        $response = array(
            'message' => array(
                'title' => 'Successful',
                'type' => 'success',
                'message' => 'User Type added successfully.'
            )
        );
        return response($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = array(
            'data' => UserType::find($id)
        );
        return response($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'name' => 'required',
            'description' => 'required',
        ));

        $user_type = UserType::find($id);
        $user_type->name = $request->name;
        $user_type->description = $request->description;
        $user_type->save();

        $response = array(
            'message' => array(
                'title' => 'Updated',
                'type' => 'info',
                'message' => 'User Type updated successfully.'
            )
        );
        return response($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
