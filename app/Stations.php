<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stations extends Model
{
    use SoftDeletes;
    public $incrementing = false;
}
